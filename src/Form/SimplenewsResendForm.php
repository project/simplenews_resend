<?php

namespace Drupal\simplenews_resend\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Simplenews resend form.
 */
class SimplenewsResendForm extends ContentEntityConfirmFormBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to reset the status of %entity?', ['%entity' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Reset');

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->resetStatus();
    $this->messenger()->addMessage($this->t('Newsletter %label has been reset.', ['%label' => $this->entity->label()]));
    \Drupal::logger('simplenews')->notice('Newsletter %label has been reset.', ['%label' => $this->entity->label()]);
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.node.canonical', ['node' => $this->entity->id()]);
  }

  /**
   * {@inheritdoc}
   */
  private function resetStatus() {
    $this->entity->simplenews_issue->status = SIMPLENEWS_STATUS_SEND_NOT;
    $this->entity->save();
  }

}
