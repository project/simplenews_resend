# Simplenews Resend module

## DESCRIPTION

Simplenews Resend is a tiny module, that allows to reset the sent status
of an already sent Simplenews newsletter issue to "not sent". This allows to
resend a newsletter (e.g. to a different category) without duplicating the node
or changing the status value by hand in the database.


## REQUIREMENTS

This module requires the following modules:
 * Simplenews (https://drupal.org/project/simplenews)


## INSTALLATION

Install as you would normally install a contributed drupal module.

## USAGE

1) Go to the Newsletter Issues (Content -> Newsletter Issues)
2) Find the "Operations Links" column on the right side of the news letter and hit the dropdown.
3) Choose "Reset Newsletter Status"
4) You will be redirected to another page where you will be asked if you are sure you want to reset the status. (Click Reset)

Screenshots: https://www.drupal.org/project/simplenews_resend/issues/3419140

## Important Notes:
- The option to reset will only show up "if" the sent newsletter has completed. If it still shows that it is trying to send newsletters, the option won't be there.
- If the newsletter has never been sent, the option won't be there.

